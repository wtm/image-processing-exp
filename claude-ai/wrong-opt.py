import cv2
import numpy as np
from skimage.morphology import skeletonize, thin
from scipy.ndimage import distance_transform_edt

def trace_text(image_path):
    img = cv2.imread(image_path)
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    _, binary = cv2.threshold(gray, 127, 255, cv2.THRESH_BINARY_INV)

    # Use thinning instead of skeletonize for faster processing
    skeleton = thin(binary / 255)

    dist_transform = distance_transform_edt(binary)

    # Find paths efficiently
    paths = find_paths(skeleton)

    # Simple ordering based on top-left position
    paths.sort(key=lambda p: (p[0][0], p[0][1]))

    return paths, img, dist_transform

def find_paths(skeleton):
    # Find endpoints and junctions
    neighbors = cv2.filter2D(skeleton.astype(np.uint8), -1, np.ones((3,3))) * skeleton
    endpoints = np.column_stack(np.where((neighbors == 2) | (neighbors == 1)))
    junctions = np.column_stack(np.where(neighbors > 3))

    visited = set()
    paths = []

    for start in endpoints:
        if tuple(start) in visited:
            continue
        path = trace_path(skeleton, start, junctions, visited)
        if len(path) > 1:
            paths.append(path)

    return paths

def trace_path(skeleton, start, junctions, visited):
    path = [start]
    current = start
    while True:
        visited.add(tuple(current))
        y, x = current
        neighbors = [(y+dy, x+dx) for dy in [-1,0,1] for dx in [-1,0,1]
                     if (dy != 0 or dx != 0) and
                     0 <= y+dy < skeleton.shape[0] and
                     0 <= x+dx < skeleton.shape[1] and
                     skeleton[y+dy, x+dx] and
                     (y+dy, x+dx) not in visited]

        if not neighbors:
            break
        elif len(neighbors) == 1 or tuple(current) in [tuple(j) for j in junctions]:
            current = neighbors[0]
            path.append(current)
        else:
            break

    return np.array(path)

def create_animation(paths, img, dist_transform, output_path, fps=30):
    height, width = img.shape[:2]
    fourcc = cv2.VideoWriter_fourcc(*'mp4v')
    out = cv2.VideoWriter(output_path, fourcc, fps, (width, height))

    mask = np.ones((height, width), dtype=np.uint8) * 255

    for path in paths:
        for i in range(1, len(path)):
            start, end = path[i-1], path[i]

            # Efficient line drawing
            cv2.line(mask, tuple(start[::-1]), tuple(end[::-1]), 0,
                     thickness=max(1, int(dist_transform[tuple(start)])))

            result = img.copy()
            result[mask == 255] = 255
            out.write(result)

    for _ in range(fps * 3):
        out.write(img)

    out.release()

# Set up the image and animation
image_path = 'math.png'
output_path = 'handwriting_animation.mp4'
paths, img, dist_transform = trace_text(image_path)

# Create the animation
create_animation(paths, img, dist_transform, output_path)

print(f"Animation saved to {output_path}")
