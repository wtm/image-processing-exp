import cv2
import numpy as np
from skimage.morphology import skeletonize
from scipy.ndimage import distance_transform_edt

def trace_text(image_path):
    # Read the image
    img = cv2.imread(image_path)
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

    # Threshold the image
    _, binary = cv2.threshold(gray, 127, 255, cv2.THRESH_BINARY_INV)

    # Skeletonize the image
    skeleton = skeletonize(binary / 255)

    # Compute distance transform
    dist_transform = distance_transform_edt(binary)

    # Find endpoints and junctions
    endpoints = find_endpoints(skeleton)
    junctions = find_junctions(skeleton)

    # Trace paths
    paths = []
    for start in endpoints:
        path = trace_path(skeleton, start, junctions)
        if len(path) > 1:  # Ignore single-pixel paths
            paths.append(np.array(path))  # Convert to numpy array here

    # Order paths
    ordered_paths = order_paths(paths)

    return ordered_paths, img, dist_transform

def find_endpoints(skeleton):
    kernel = np.array([[1, 1, 1],
                       [1, 10, 1],
                       [1, 1, 1]])
    conv = cv2.filter2D(skeleton.astype(np.uint8), -1, kernel)
    return np.column_stack(np.where(conv == 11))

def find_junctions(skeleton):
    kernel = np.array([[1, 1, 1],
                       [1, 10, 1],
                       [1, 1, 1]])
    conv = cv2.filter2D(skeleton.astype(np.uint8), -1, kernel)
    return np.column_stack(np.where(conv >= 13))

def trace_path(skeleton, start, junctions):
    path = [start]
    current = start
    while True:
        neighbors = get_neighbors(current, skeleton)
        neighbors = [n for n in neighbors if tuple(n) not in [tuple(p) for p in path]]
        if len(neighbors) == 0:
            break
        elif len(neighbors) == 1 or tuple(current) in [tuple(j) for j in junctions]:
            current = neighbors[0]
            path.append(current)
        else:
            break
    return path

def get_neighbors(point, skeleton):
    y, x = point
    neighbors = []
    for dy in [-1, 0, 1]:
        for dx in [-1, 0, 1]:
            if dy == 0 and dx == 0:
                continue
            ny, nx = y + dy, x + dx
            if 0 <= ny < skeleton.shape[0] and 0 <= nx < skeleton.shape[1] and skeleton[ny, nx]:
                neighbors.append([ny, nx])
    return neighbors

def order_paths(paths):
    # Convert paths to a list of tuples for easier handling
    paths = [tuple(map(tuple, path)) for path in paths]

    # Sort paths from top to bottom, then left to right
    paths.sort(key=lambda p: (p[0][0], p[0][1]))

    ordered_paths = []
    while paths:
        if not ordered_paths:
            ordered_paths.append(paths.pop(0))
        else:
            last_end = ordered_paths[-1][-1]
            closest_path = min(paths, key=lambda p: np.linalg.norm(np.array(p[0]) - np.array(last_end)))
            paths.remove(closest_path)

            # Determine if the path needs to be reversed
            if np.linalg.norm(np.array(closest_path[0]) - np.array(last_end)) > np.linalg.norm(np.array(closest_path[-1]) - np.array(last_end)):
                closest_path = closest_path[::-1]

            ordered_paths.append(closest_path)

    # Convert back to numpy arrays
    ordered_paths = [np.array(path) for path in ordered_paths]

    return ordered_paths

def create_animation(paths, img, dist_transform, output_path, fps=30):
    height, width = img.shape[:2]
    fourcc = cv2.VideoWriter_fourcc(*'mp4v')
    out = cv2.VideoWriter(output_path, fourcc, fps, (width, height))

    # Create a blank (white) mask
    mask = np.ones((height, width), dtype=np.uint8) * 255

    for path in paths:
        for i in range(1, len(path)):
            start = path[i-1]
            end = path[i]

            # Interpolate points between start and end
            num_points = max(abs(end[0] - start[0]), abs(end[1] - start[1])) * 2
            print(num_points)
            y_coords = np.linspace(start[0], end[0], num_points).astype(int)
            x_coords = np.linspace(start[1], end[1], num_points).astype(int)

            for y, x in zip(y_coords, x_coords):
                # Get the radius from the distance transform
                radius = int(dist_transform[y, x])

                # Update mask
                cv2.circle(mask, (x, y), radius, 0, -1)

                # Apply mask to original image
                result = img.copy()
                result[mask == 255] = 255

                out.write(result)

    # Add a few seconds of the completed image at the end
    for _ in range(fps * 3):
        out.write(img)

    out.release()

# Set up the image and animation
image_path = 'math.png'
output_path = 'handwriting_animation.mp4'
paths, img, dist_transform = trace_text(image_path)

# Create the animation
create_animation(paths, img, dist_transform, output_path)

print(f"Animation saved to {output_path}")
