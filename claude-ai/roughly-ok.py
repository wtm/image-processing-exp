import cv2
import numpy as np
from skimage.morphology import skeletonize
from scipy.ndimage import distance_transform_edt

def trace_text(image_path):
    img = cv2.imread(image_path)
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    _, binary = cv2.threshold(gray, 127, 255, cv2.THRESH_BINARY_INV)

    skeleton = skeletonize(binary / 255)
    dist_transform = distance_transform_edt(binary)

    paths = find_connected_components(skeleton)

    # Sort components from left to right, then top to bottom
    paths.sort(key=lambda p: (np.min(p[:, 1]), np.min(p[:, 0])))

    return paths, img, dist_transform

def find_connected_components(skeleton):
    num_labels, labels = cv2.connectedComponents(skeleton.astype(np.uint8))
    paths = []
    for label in range(1, num_labels):  # Start from 1 to skip background
        component = np.array(np.where(labels == label)).T
        # Order points within each component
        ordered_component = order_points_in_component(component)
        paths.append(ordered_component)
    return paths

def order_points_in_component(component):
    # Find the topmost-leftmost point as starting point
    start = component[np.lexsort((component[:, 1], component[:, 0]))][0]
    ordered = [start]
    remaining = set(map(tuple, component)) - {tuple(start)}

    while remaining:
        last = np.array(ordered[-1])
        neighbors = [p for p in remaining if np.linalg.norm(np.array(p) - last) <= np.sqrt(2)]
        if not neighbors:
            # If no direct neighbors, find the closest remaining point
            closest = min(remaining, key=lambda p: np.linalg.norm(np.array(p) - last))
            ordered.append(closest)
            remaining.remove(closest)
        else:
            # Choose the neighbor that's most in line with the current direction
            if len(ordered) > 1:
                direction = np.array(ordered[-1]) - np.array(ordered[-2])
                next_point = max(neighbors, key=lambda p: np.dot(direction, np.array(p) - last))
            else:
                next_point = neighbors[0]
            ordered.append(next_point)
            remaining.remove(tuple(next_point))

    return np.array(ordered)

def create_animation(paths, img, dist_transform, output_path, fps=30):
    height, width = img.shape[:2]
    fourcc = cv2.VideoWriter_fourcc(*'mp4v')
    out = cv2.VideoWriter(output_path, fourcc, fps, (width, height))

    mask = np.ones((height, width), dtype=np.uint8) * 255

    for path in paths:
        for i in range(1, len(path)):
            start, end = path[i-1], path[i]

            num_points = max(abs(end[0] - start[0]), abs(end[1] - start[1])) * 2
            y_coords = np.linspace(start[0], end[0], num_points).astype(int)
            x_coords = np.linspace(start[1], end[1], num_points).astype(int)

            for y, x in zip(y_coords, x_coords):
                radius = max(1, int(dist_transform[y, x]))
                cv2.circle(mask, (x, y), radius, 0, -1)

                result = img.copy()
                result[mask == 255] = 255
                out.write(result)

    for _ in range(fps * 3):
        out.write(img)

    out.release()

# Set up the image and animation
image_path = 'math.png'
output_path = 'handwriting_animation.mp4'
paths, img, dist_transform = trace_text(image_path)

# Create the animation
create_animation(paths, img, dist_transform, output_path)

print(f"Animation saved to {output_path}")
