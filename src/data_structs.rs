pub struct Simple2DArray<T> {
	pub width: u32,
	pub height: u32,
	pub data: Vec<T>
}

impl<T> Simple2DArray<T> {
	pub fn entry(&self, x: u32, y: u32) -> &T {
		debug_assert!(x < self.width && y < self.height);
		&self.data[(y*self.width + x) as usize]
	}

	pub fn entry_mut(&mut self, x: u32, y: u32) -> &mut T {
		debug_assert!(x < self.width && y < self.height);
		&mut self.data[(y*self.width + x) as usize]
	}
}
