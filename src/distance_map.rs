use crate::data_structs::Simple2DArray;

pub fn compute_distance_map(input: &Simple2DArray<bool>) -> Simple2DArray<f32> {
	use crate::coordinate_patterns::borders::diamond as diamond_border;

	let mut output = Simple2DArray{
		width: input.width,
		height: input.height,
		data: Vec::with_capacity((input.width * input.height) as usize)
	};
	for y in 0..input.height {
		for x in 0..input.width {
			let mut found_distance: Option<f32> = None;
			let max_distance = if input.width > input.height {
				input.width
			} else {
				input.height
			};
			for manhatten_dist in 0..max_distance {
				let mut have_valid_pixels = false;
				for offset in diamond_border(manhatten_dist) {
					let (cx, cy) = (x as i32 + offset.0, y as i32 + offset.1);
					if cx < 0 || cy < 0 || cx >= input.width as i32 || cy >= input.height as i32 {
						continue;
					}
					have_valid_pixels = true;
					if !input.entry(cx as u32, cy as u32) {
						let dist = f32::sqrt(f32::powi(x as f32 - cx as f32, 2) + f32::powi(y as f32 - cy as f32, 2));
						match found_distance {
							None => {
								found_distance = Some(dist);
							},
							Some(found_dist) => {
								if found_dist > dist {
									found_distance = Some(dist);
								}
							}
						};
					}
				}
				if !have_valid_pixels {
					break;
				}
				if let Some(found_dist) = found_distance {
					if manhatten_dist as f32 > found_dist as f32 * f32::sqrt(2f32) {
						break;
					}
				}
			}
			output.data.push(found_distance.unwrap_or(max_distance as f32));
		}
	}
	debug_assert_eq!(output.data.len(), (output.width * output.height) as usize);
	output
}
