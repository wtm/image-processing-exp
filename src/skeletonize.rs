use crate::data_structs::Simple2DArray;
use image::{ImageBuffer, Luma};
use crate::coordinate_patterns::skeleton_coordinate;

pub struct SkeletonResult {
	image_dim: (u32, u32),
	distances: Vec<Option<f32>>
}

impl SkeletonResult {
	fn new(image_dimensions: (u32, u32)) -> Self {
		Self{
			distances: vec![None; skeleton_coordinate::flattened_length(image_dimensions)],
			image_dim: image_dimensions
		}
	}

	pub fn image_dimensions(&self) -> (u32, u32) {
		self.image_dim
	}

	pub fn get(&self, x: f32, y: f32) -> &Option<f32> {
		&self.distances[skeleton_coordinate::flatten(self.image_dim, (x, y))]
	}

	fn get_mut(&mut self, x: f32, y: f32) -> &mut Option<f32> {
		let i = skeleton_coordinate::flatten(self.image_dim, (x, y));
		&mut self.distances[i]
	}

	fn insert(&mut self, coord: (f32, f32), distance: f32) {
		let entry = self.get_mut(coord.0, coord.1);
		if let Some(stored_dist) = entry {
			if *stored_dist != distance {
				// FIXME
				*stored_dist = (*stored_dist + distance) / 2f32;
				return;
				// panic!("BUG: Double insert with different distances? {:?}: Has {}, inserting {}.", coord, stored_dist, distance);
			}
		}
		*entry = Some(distance)
	}

	pub fn debug_image(&self) -> ImageBuffer<Luma<u8>, Vec<u8>> {
		let w = self.image_dim.0 * 2 + 1;
		let h = self.image_dim.1 * 2 + 1;
		let mut out_image = ImageBuffer::new(w, h);
		for y in 0..h {
			for x in 0..w {
				let val = match self.distances[(y*w+x) as usize] {
					None => 0u8,
					Some(dist) => 55u8 + (if dist > 200f32 {
						200f32
					} else {
						dist
					} as u8)
				};
				out_image.put_pixel(x, y, Luma([val]));
			}
		}
		out_image
	}
}

pub fn skeleton_from_distance_map(dmap: &Simple2DArray<f32>) -> SkeletonResult {
	let mut result = SkeletonResult::new((dmap.width, dmap.height));
	if dmap.width <= 2 || dmap.height <= 2 {
		return result;
	}
	for y in 2..dmap.height-2 {
		for x in 2..dmap.width-2 {
			if *dmap.entry(x, y) == 0f32 {
				continue;
			}

			let mut do_strip = |adjs: Vec<f32>, coords: [(f32, f32); 3]| {
				assert_eq!(adjs.len(), 5);
				let max = *adjs[1..=3].iter().max_by(|a, b| PartialOrd::partial_cmp(a, b).unwrap()).unwrap();
				if adjs[2] == max {
					if adjs[1] == max && adjs[3] < max && adjs[0] < max {
						result.insert(coords[0], max + 0.5f32);
					} else if adjs[3] == max && adjs[1] < max && adjs[4] < max {
						result.insert(coords[2], max + 0.5f32);
					} else if adjs[1] < max && adjs[3] < max {
						result.insert(coords[1], max);
					} else {
						// not a turning point since all three equal.
					}
				}
			};
			let xi = x as i32;
			let yi = y as i32;
			let x = x as f32;
			let y = y as f32;
			do_strip(((-2)..=2).map(|off| *dmap.entry(xi as u32, (yi + off) as u32)).collect(), [(x, y - 0.5f32), (x, y), (x, y + 0.5f32)]);
			do_strip(((-2)..=2).map(|off| *dmap.entry((xi + off) as u32, yi as u32)).collect(), [(x - 0.5f32, y), (x, y), (x + 0.5f32, y)]);
			do_strip(((-2)..=2).map(|off| *dmap.entry((xi + off) as u32, (yi + off) as u32)).collect(), [(x - 0.5f32, y - 0.5f32), (x, y), (x + 0.5f32, y + 0.5f32)]);
			do_strip(((-2)..=2).map(|off| *dmap.entry((xi + off) as u32, (yi - off) as u32)).collect(), [(x - 0.5f32, y + 0.5f32), (x, y), (x + 0.5f32, y - 0.5f32)]);
		}
	}



	result
}
