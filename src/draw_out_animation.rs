use crate::skeletonize::SkeletonResult;
use crate::coordinate_patterns::{skeleton_coordinate, borders};
use image::{ImageBuffer, Luma};
use std::iter::Iterator;

pub struct DrawOutAnimation {
	pub img_dim: (u32, u32),
	pub nodes: Vec<Node>,
	pub strokes: Vec<Stroke>,
	nb_frames: Option<u32>
}

#[derive(Debug)]
pub struct Node {
	pub x: f32,
	pub y: f32,
	pub radius: f32,
	pub neighbours: Vec<(f32, f32)>,
	pub stroke_ids: Vec<u32>,
	pub frame_number: Option<u32>
}

impl Node {
	fn new(x: f32, y: f32, radius: f32) -> Node {
		Node{
			x, y, radius,
			neighbours: Vec::new(),
			stroke_ids: Vec::new(),
			frame_number: None
		}
	}
}

#[derive(Debug)]
pub struct Stroke {
	id: u32,
	points: Vec<(f32, f32)>
}

impl DrawOutAnimation {
	pub fn from_skeleton(skeleton: SkeletonResult) -> Self {
		let img_dim = skeleton.image_dimensions();
		macro_rules! c2i {
			($c:expr) => {
				skeleton_coordinate::flatten(img_dim, $c)
			};
		}
		let mut graph_nodes = Vec::with_capacity(skeleton_coordinate::flattened_length(img_dim));
		let mut node_coordinates = Vec::new();

		// construct node objects
		for point in skeleton_coordinate::iter(img_dim) {
			let i = c2i!(point);
			assert_eq!(graph_nodes.len(), i);
			let (x, y) = point;
			let sk_entry = skeleton.get(x, y);
			if let Some(dist) = *sk_entry {
				let node = Node::new(x, y, dist);
				graph_nodes.push(Some(node));
				node_coordinates.push(point);
			} else {
				graph_nodes.push(None);
			}
		}

		// connect neighbour nodes together.
		let connect = |graph_nodes: &mut Vec<Option<Node>>, p1: &(f32, f32), p2: &(f32, f32)| {
			assert_ne!(p1, p2);
			let i1 = c2i!(*p1);
			let i2 = c2i!(*p2);
			if graph_nodes[i1].as_ref().unwrap().neighbours.contains(p2) {
				return;
			}
			graph_nodes[i1].as_mut().unwrap().neighbours.push(*p2);
			let their_neighbours = &mut graph_nodes[i2].as_mut().unwrap().neighbours;
			assert!(!their_neighbours.contains(p1));
			their_neighbours.push(*p1);
		};

		let strategies: Vec<Box<dyn Fn() -> Box<dyn Iterator<Item = (i32, i32)>>>> =
			vec![
				Box::new(|| Box::new(borders::diamond(1))),
				Box::new(|| Box::new(borders::square(3))),
				Box::new(|| Box::new(borders::square(5)))
			];
		for connection_strategy in strategies {
			for point in node_coordinates.iter() {
				for offset in connection_strategy() {
					// test if already (indirectly) connected
					let node_coord = (point.0 + offset.0 as f32 / 2f32, point.1 + offset.1 as f32 / 2f32);
					if graph_nodes[c2i!(node_coord)].is_some() && graph_nodes[c2i!(*point)].as_ref().unwrap().neighbours.iter().map(|p| *p) // direct neighbours
									.flat_map(|p1| graph_nodes[c2i!(p1)].as_ref().unwrap().neighbours.iter().map(|p| *p).chain(std::iter::once(p1))) // a->b->c
									.flat_map(|p1| graph_nodes[c2i!(p1)].as_ref().unwrap().neighbours.iter().map(|p| *p).chain(std::iter::once(p1))) // a->b->c->d
									.find(|p2| p2 == &node_coord).is_none() {
						connect(&mut graph_nodes, &point, &node_coord);
					}
				}
			}
		}

		// construct strokes
		let mut strokes = Vec::new();
		loop {
			// start with preferably a node with only one connecting edge. This is likely to be a natural starting point
			// for a stroke.
			let starting_node = node_coordinates.iter().map(|x| graph_nodes[c2i!(*x)].as_ref().unwrap())
														.filter(|x| x.stroke_ids.is_empty())
														.min_by(|a, b| a.neighbours.len().cmp(&b.neighbours.len()));
			if starting_node.is_none() {
				break;
			}
			let starting_node = starting_node.unwrap();
			let stroke_id = strokes.len() as u32;
			strokes.push(Stroke{
				id: stroke_id,
				points: Vec::new()
			});
			let stroke = strokes.last_mut().unwrap();
			use std::collections::VecDeque;
			#[derive(Debug)]
			struct QueueItem {
				point: (f32, f32),
				trail: VecDeque<(f32, f32)>
			}
			const TRAIL_LIMIT: usize = 5;
			let mut stroke_queue = Vec::new();
			let stroke_start_point = (starting_node.x, starting_node.y);
			stroke_queue.push(QueueItem{
				point: stroke_start_point,
				trail: VecDeque::new()
			});
			while let Some(cont_from) = stroke_queue.pop() {
				let cont_i = c2i!(cont_from.point);
				let node = &mut graph_nodes[cont_i].as_mut().unwrap();
				if node.stroke_ids.contains(&stroke_id) {
					// happens when node gets added multiple times into the queue
					continue;
				}
				node.stroke_ids.push(stroke_id);
				stroke.points.push((node.x, node.y));
				let node = graph_nodes[cont_i].as_ref().unwrap();
				// note that two strokes can cross the same point, so we also looks at points that have been crossed by previous strokes, as long
				// as it has not been crossed by this stroke yet.
				let unstroked_branches_filter = |coord: &(f32, f32)| {
					let node = graph_nodes[c2i!(*coord)].as_ref().unwrap();
					if node.neighbours.len() <= 2 {
						return node.stroke_ids.is_empty();
					}
					if !node.stroke_ids.contains(&stroke_id) {
						return true;
					}
					// // allow self cross if it leds to other uncovered nodes
					// if node.neighbours.iter().any(|np| {
					// 	let neighbour_node = &graph_nodes[c2i!(*np)].as_ref().unwrap();
					// 	neighbour_node.stroke_ids.is_empty()
					// }) {
					// 	return true;
					// }
					return false;
				};
				let mut trail = cont_from.trail;
				if trail.len() >= TRAIL_LIMIT {
					trail.pop_front();
				}
				trail.push_back(cont_from.point);
				let current_direction = if trail.len() < 2 {
					None
				} else {
					let first = trail[0];
					let last = trail[trail.len() - 1];
					let d = (last.0 - first.0, last.1 - first.1);
					let m = f32::sqrt(d.0.powi(2) + d.1.powi(2));
					if m > 0f32 {
						Some((d.0 / m, d.1 / m))
					} else {
						unreachable!() // trail can't contain duplicates
					}
				};
				let branches: Vec<(f32, f32)> = node.neighbours.iter().map(|x| *x).filter(unstroked_branches_filter).collect();
				match branches.len() {
					0 => {
						// either a single point, or this stroke ends here.
					},
					1 => {
						let next = branches[0];
						stroke_queue.push(QueueItem{
							point: next,
							trail
						});
					},
					_ => {
						let generally_heading_towards: Vec<_> = branches.iter().map(|x| *x).map(|point| {
							const WALK_AHEAD: usize = 5;
							struct QueueItem {
								last: (f32, f32),
								current: (f32, f32)
							}
							let mut queue = vec![QueueItem{
								last: point,
								current: point
							}];
							for _ in 0..WALK_AHEAD {
								let mut new_queue = Vec::new();
								for n in queue.iter() {
									let QueueItem{last, current} = n;
									let branches: Vec<(f32, f32)> = graph_nodes[c2i!(*current)].as_ref().unwrap()
																										.neighbours.iter().map(|x| *x)
																										.filter(unstroked_branches_filter)
																										.filter(|p| p != last).collect();
									for b in branches {
										let last = *current;
										let current = b;
										new_queue.push(QueueItem{last, current});
									}
								}
								queue = new_queue;
							}
							queue.into_iter().map(|q| {
								let v1 = q.current;
								let v0 = point;
								let d = (v1.0 - v0.0, v1.1 - v0.1);
								let m = f32::sqrt(d.0.powi(2) + d.1.powi(2));
								if m > 0f32 {
									(d.0 / m, d.1 / m)
								} else {
									(0f32, 0f32)
								}
							}).max_by(|d1, d2| {
								let cross = |d: &(f32, f32)| {
									if let Some(current_direction) = current_direction {
										current_direction.0 * d.0 + current_direction.1 * d.1
									} else {
										f32::sqrt(d.0.powi(2) + d.1.powi(2))
									}
								};
								cross(d1).partial_cmp(&cross(d2)).unwrap()
							})
						}).collect();
						debug_assert_eq!(branches.len(), generally_heading_towards.len());
						let mut cont_branch_candidates = branches.iter().zip(generally_heading_towards.iter()).filter(|&(_, d)| d.is_some()).map(|(a, b)| (a, b.unwrap()));
						let cont_branch: Option<(f32, f32)>;
						if let Some(current_direction) = current_direction {
							cont_branch = cont_branch_candidates.map(|(branch, dir)| {
								(branch, (current_direction.0 * dir.0 + current_direction.1 * dir.1))
							}).max_by(|&(_, a), &(_, b)| {
								a.partial_cmp(&b).unwrap()
							}).map(|x| {
								*x.0
							});
						} else {
							cont_branch = cont_branch_candidates.next().map(|x| *x.0);
						}
						if let Some(cont_branch) = cont_branch {
							stroke_queue.push(QueueItem{
								point: cont_branch,
								trail: trail.clone()
							});
						}
						for (branch, direction) in branches.iter().map(|x| *x).zip(generally_heading_towards.iter()) {
							if direction.is_none() {
								stroke_queue.push(QueueItem{
									point: branch,
									trail: trail.clone()
								});
							}
						}
					}
				}
			}
		}

		let mut o = Self{
			img_dim, strokes,
			nodes: graph_nodes.into_iter().filter(|x| x.is_some()).map(|x| x.unwrap()).collect(),
			nb_frames: None
		};
		o.to_frames();
		o
	}

	pub fn debug_image(&self) -> ImageBuffer<Luma<u8>, Vec<u8>> {
		let mut result_image = ImageBuffer::new(self.img_dim.0, self.img_dim.1);
		for p in result_image.pixels_mut() {
			*p = Luma([255u8]);
		}
		for n in self.nodes.iter() {
			let pix_val = Luma([((n.frame_number.unwrap() as f32 / self.nb_frames.unwrap() as f32) * 255f32) as u8]);
			for pix_coord in crate::coordinate_patterns::areas::circle(n.x, n.y, n.radius) {
				if pix_coord.0 < 0 || pix_coord.1 < 0 {
					continue;
				}
				let pix_coord = (pix_coord.0 as u32, pix_coord.1 as u32);
				if result_image.get_pixel(pix_coord.0, pix_coord.1).0[0] > pix_val.0[0] {
					result_image.put_pixel(pix_coord.0, pix_coord.1, pix_val);
				}
			}
		}
		result_image
	}

	pub fn visualize_graph(&self) -> String {
		use std::fmt::Write;
		use rand::{RngCore, SeedableRng, rngs::SmallRng};
		let mut rng = SmallRng::seed_from_u64(0u64);
		const ERRMSG: &str = "unable to format stuff to string";
		let mut result = String::new();
		let mut result2 = String::new();
		write!(result, r#"<svg viewBox="0 0 {} {}">"#, self.img_dim.0, self.img_dim.1).expect(ERRMSG);
		for n in self.nodes.iter() {
			write!(result, r#"<circle cx="{}" cy="{}" r="{}" style="fill: red; stroke: none;" data-stroke-ids="{:?}" />"#, n.x, n.y, n.radius.max(10f32)*0.01f32, n.stroke_ids).expect(ERRMSG);
			for neighbour in n.neighbours.iter().map(|x| *x) {
				if neighbour.1 > n.y || (neighbour.1 == n.y && neighbour.0 > n.x) {
					// don't duplicate lines
					let scolor = format!("rgb({}, {}, {})", rng.next_u32() & 200, rng.next_u32() % 200, rng.next_u32() % 200);
					write!(result2, r#"<path d="M {} {} L {} {}" style="fill: none; stroke: {stroke_color}; stroke-width: 0.01; stroke-dasharray: 0.05; />"#, n.x, n.y, neighbour.0, neighbour.1, stroke_color = scolor).expect(ERRMSG);
				}
			}
		}
		result.push_str(&result2);
		for stroke in self.strokes.iter() {
			if stroke.points.len() < 2 {
				continue;
			}
			let mut d = String::new();
			write!(d, "M {} {}", stroke.points[0].0, stroke.points[0].1).expect(ERRMSG);
			for point in stroke.points.iter().skip(1) {
				write!(d, " L {} {}", point.0, point.1).expect(ERRMSG);
			}
			write!(result, r#"<path id="stroke{}" d={:?} style="fill: none; stroke: black; stroke-width: 0.02;" />"#, stroke.id, &d).expect(ERRMSG);
		}
		write!(result, r#"</svg>"#).expect(ERRMSG);
		result
	}

	pub fn to_frames(&mut self) {
		use std::iter;
		use std::iter::FromIterator;
		if self.nb_frames.is_some() {
			return;
		}
		let mut strokes: Vec<&Stroke> = self.strokes.iter().collect();
		strokes.sort_unstable_by_key(|s| (s.points.iter().map(|x| x.0).sum::<f32>() / s.points.len() as f32) as i32);
		let mut node_graph: Vec<Option<&mut Node>> = Vec::from_iter(iter::repeat_with(|| None).take(skeleton_coordinate::flattened_length(self.img_dim)));
		macro_rules! c2i {
			($c:expr) => {
				skeleton_coordinate::flatten(self.img_dim, $c)
			};
		}
		for n in self.nodes.iter_mut() {
			let p = (n.x, n.y);
			node_graph[c2i!(p)] = Some(n);
		}
		let mut f = 0u32;
		let mut max_f = 0u32;
		for s in strokes {
			let mut ff = f;
			for point in s.points.iter() {
				let node = node_graph[c2i!(*point)].as_mut().unwrap();
				if node.frame_number.is_none() {
					node.frame_number = Some(ff);
					max_f = max_f.max(ff);
					ff += 1;
				}
			}
			f = ff;
		}
		self.nb_frames = Some(max_f);
	}
}
