pub fn flatten(image_dimensions: (u32, u32), skeleton_point: (f32, f32)) -> usize {
	let (x, y) = skeleton_point;
	let (w, h) = image_dimensions;
	if x.fract().abs() != 0f32 && x.fract().abs() != 0.5f32 {
		panic!("Invalid x");
	}
	if y.fract().abs() != 0f32 && y.fract().abs() != 0.5f32 {
		panic!("Invalid y");
	}
	if x < -0.5f32 || x > w as f32 - 0.5f32 {
		panic!("x out of range");
	}
	if y < -0.5f32 || y > h as f32 - 0.5f32 {
		panic!("y out of range");
	}
	let i_x = (x + 0.5f32) * 2f32;
	let i_y = (y + 0.5f32) * 2f32;
	if i_x.fract() != 0f32 || i_y.fract() != 0f32 || i_x < 0f32 || i_y < 0f32 {
		unreachable!();
	}
	i_y as usize * (2 * w as usize + 1) + i_x as usize
}

pub fn flattened_length(image_dimension: (u32, u32)) -> usize {
	((image_dimension.0 * 2 + 1) * (image_dimension.1 * 2 + 1)) as usize
}

struct SkeletonCoordinateIterator {
	image_width: u32,
	image_height: u32,
	current_x: f32,
	current_y: f32,
}

impl Iterator for SkeletonCoordinateIterator {
	type Item = (f32, f32);

	fn next(&mut self) -> Option<(f32, f32)> {
		if self.current_y > self.image_height as f32 - 0.5f32 {
			None
		} else {
			let ret = (self.current_x, self.current_y);
			self.current_x += 0.5f32;
			if self.current_x > self.image_width as f32 - 0.5f32 {
				self.current_x = -0.5f32;
				self.current_y += 0.5f32;
			}
			Some(ret)
		}
	}
}

pub fn iter(image_dimensions: (u32, u32)) -> impl Iterator<Item = (f32, f32)> {
	SkeletonCoordinateIterator{
		image_width: image_dimensions.0,
		image_height: image_dimensions.1,
		current_x: -0.5f32,
		current_y: -0.5f32,
	}
}
