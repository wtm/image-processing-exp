use std::iter;

pub fn diamond(n: u32) -> impl Iterator<Item = (i32, i32)> {
	let mut cx = -(n as i32);
	let mut cy = 0;
	let mut done = false;
	let mut mode: i32 = 0;
	iter::from_fn(move || {
		if done {
			return None;
		}
		if n == 0 {
			done = true;
			return Some((0i32, 0i32));
		}
		let ret = (cx, cy);
		match mode {
			0 => {
				cx += 1;
				cy += 1;
				if cx >= 0 {
					mode = 1;
				}
			}
			1 => {
				cy -= 1;
				cx += 1;
				if cy <= 0 {
					mode = 2;
				}
			}
			2 => {
				cx -= 1;
				cy -= 1;
				if cx <= 0 {
					mode = 3;
				}
			}
			3 => {
				cx -= 1;
				cy += 1;
				if cy >= 0 {
					done = true;
				}
			}
			_ => unreachable!()
		}
		Some(ret)
	})
}

#[test]
fn test_diamond() {
	assert_eq!(
		diamond(0).collect::<Vec<_>>(),
		vec![(0, 0)]
	);
	assert_eq!(
		diamond(1).collect::<Vec<_>>(),
		vec![(-1, 0), (0, 1), (1, 0), (0, -1)]
	);
	assert_eq!(
		diamond(2).collect::<Vec<_>>(),
		vec![(-2, 0), (-1, 1), (0, 2), (1, 1), (2, 0), (1, -1), (0, -2), (-1, -1)]
	);
}

pub fn square(side_length: u32) -> impl Iterator<Item = (i32, i32)> {
	if side_length % 2 != 1 {
		panic!("side_length must be odd.");
	}
	let mut ret = vec![];
	if side_length == 1 {
		ret.push((0i32, 0i32));
		return ret.into_iter();
	}
	let n = side_length / 2;
	let mut cx = -(n as i32);
	let mut cy = n as i32;
	while cx < n as i32 {
		ret.push((cx, cy));
		cx += 1;
	}
	debug_assert_eq!((cx, cy), (n as i32, n as i32));
	while cy > -(n as i32) {
		ret.push((cx, cy));
		cy -= 1;
	}
	debug_assert_eq!((cx, cy), (n as i32, -(n as i32)));
	while cx > -(n as i32) {
		ret.push((cx, cy));
		cx -= 1;
	}
	debug_assert_eq!((cx, cy), (-(n as i32), -(n as i32)));
	while cy < n as i32 {
		ret.push((cx, cy));
		cy += 1;
	}
	debug_assert_eq!((cx, cy), (-(n as i32), n as i32));
	return ret.into_iter();
}

#[test]
fn test_square() {
	assert_eq!(
		square(1).collect::<Vec<_>>(),
		vec![(0, 0)]
	);
	assert_eq!(
		square(3).collect::<Vec<_>>(),
		vec![(-1, 1), (0, 1), (1, 1), (1, 0), (1, -1), (0, -1), (-1, -1), (-1, 0)]
	);
}
