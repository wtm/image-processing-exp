use std::iter;

pub fn circle(cx: f32, cy: f32, r: f32) -> impl Iterator<Item = (i32, i32)> {
	let mut ret = vec![];
	for y in ((cy - r).floor() as i32)..((cy + r).ceil() as i32) {
		for x in ((cx - r).floor() as i32)..((cx + r).ceil() as i32) {
			if f32::powi(x as f32 - cx, 2) + f32::powi(y as f32 - cy, 2) <= f32::powi(r, 2) {
				ret.push((x, y));
			}
		}
	}
	return ret.into_iter();
}
