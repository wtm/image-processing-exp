mod distance_map;
mod coordinate_patterns;
mod data_structs;
mod skeletonize;
mod draw_out_animation;

fn main() {
	use clap::{App, Arg};
	let args = App::new("image-processing-exp")
		.arg(Arg::with_name("in")
					.index(1)
					.required(true)
					.help("Input image"))
		.arg(Arg::with_name("out_distance")
					.long("dump-distance")
					.required(false)
					.takes_value(true)
					.help("Dump distance map to image"))
		.arg(Arg::with_name("out_skeleton")
					.long("dump-skeleton")
					.required(false)
					.takes_value(true)
					.help("Dump skeleton to image"))
		.arg(Arg::with_name("out_appear_order")
					.long("dump-appear-order-map")
					.required(false)
					.takes_value(true)
					.help("Dump appear order distribution as image"))
		.arg(Arg::with_name("out_graph_svg")
					.long("visualize-graph")
					.required(false)
					.takes_value(true)
					.help("Dump node graph to svg."))
		.get_matches();
	match start(args) {
		Err(e) => {
			eprintln!("{}", e);
			std::process::exit(1);
		},
		Ok(()) => {}
	};
}

fn start<'a>(args: clap::ArgMatches<'a>) -> Result<(), String> {
	let in_path = args.value_of_os("in").unwrap();
	let in_img = image::open(in_path).map_err(|e| format!("Can't open {:?}: {}", in_path, e))?.into_luma();
	use data_structs::Simple2DArray;
	let (width, height) = in_img.dimensions();
	let mut binary_arr = Simple2DArray{
		width, height, data: Vec::with_capacity((width * height) as usize)
	};
	for y in 0..height {
		for x in 0..width {
			binary_arr.data.push(in_img.get_pixel(x, y)[0] > 127);
		}
	}
	let dmap = distance_map::compute_distance_map(&binary_arr);
	use image::{ImageBuffer, Luma};
	if let Some(distance_out) = args.value_of_os("out_distance") {
		let max_dist = *dmap.data.iter().max_by(|a, b| std::cmp::PartialOrd::partial_cmp(a, b).unwrap()).unwrap();
		let img = ImageBuffer::from_fn(width, height, |x, y| {
			let result = dmap.entry(x, y) / max_dist * 255f32;
			Luma([result as u8])
		});
		img.save(distance_out).map_err(|e| format!("Can't save to {:?}: {}", distance_out, e))?;
	}
	let skeleton = skeletonize::skeleton_from_distance_map(&dmap);
	if let Some(skeleton_out) = args.value_of_os("out_skeleton") {
		skeleton.debug_image().save(skeleton_out).map_err(|e| format!("Can't save to {:?}: {}", skeleton_out, e))?;
	}
	let anim = draw_out_animation::DrawOutAnimation::from_skeleton(skeleton);
	if let Some(map_out) = args.value_of_os("out_appear_order") {
		anim.debug_image().save(map_out).map_err(|e| format!("Can't save to {:?}: {}", map_out, e))?;
	}
	if let Some(svg_out) = args.value_of_os("out_graph_svg") {
		use std::fs;
		fs::write(svg_out, anim.visualize_graph().as_bytes()).map_err(|e| format!("Can't save to {:?}: {}", svg_out, e))?;
	}
	Ok(())
}
